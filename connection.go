package main

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
)

//getConnection
func getConnection() *sql.DB {
	//nombre del motor://nombre de usuario//contraseña@servidor:puerto/nombre de bd?conexión de manera segura a la bd
	dataSourceName := "postgres://postgres:admin@127.0.0.1:5432/gocrud?sslmode=disable"
	db, err := sql.Open("postgres", dataSourceName)
	if err != nil {
		log.Fatal(err)
	}

	return db
}
