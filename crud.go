package main

import (
	"fmt"
	"log"
)

func main() {
	e := Student{
		Name: "Alejandro",
		Age:30,
		Active: true,
	}

	err := Create(e)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Creado exitosamente")
}