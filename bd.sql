CREATE USER golang PASSWORD 'golang';
CREATE DATABASE gocrud OWNER golang;
CREATE TABLE estudiantes (
    id SERIAL NOT NULL,
    name VARCHAR(50) NOT NULL,
    age SMALLINT NOT NULL,
    active BOOLEAN NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP
);

CREATE TABLE students (
    id SERIAL NOT NULL,
    name VARCHAR(50),
    age SMALLINT,
    active BOOLEAN,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
updated_at TIMESTAMP
);